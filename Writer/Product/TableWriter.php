<?php

namespace Naolis\Bundle\ConnectorBundle\Writer\Product;

use Akeneo\Bundle\BatchBundle\Entity\StepExecution;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Schema\TableDiff;
use Doctrine\DBAL\Types\Type;
use Naolis\Bundle\ConnectorBundle\Converter\Product\ItemExpandLocalesConverter;
use Pim\Bundle\BaseConnectorBundle\Validator\Constraints\Channel as ChannelConstraint;
use Pim\Bundle\BaseConnectorBundle\Writer\File\CsvProductWriter;
use Pim\Bundle\CatalogBundle\Entity\Channel;
use Pim\Bundle\CatalogBundle\Manager\ChannelManager;
use Pim\Bundle\CatalogBundle\Manager\MediaManager;
use Symfony\Component\Validator\Constraints as Assert;

class TableWriter extends CsvProductWriter
{
    /**
     * @var ChannelManager
     */
    protected $channelManager;

    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var AbstractSchemaManager
     */
    protected $schemaManager;

    /**
     * @var ItemExpandLocalesConverter
     */
    protected $itemExpandLocalesConverter;

    /**
     * @var string|Channel
     *
     * @Assert\NotBlank(groups={"Execution"})
     * @ChannelConstraint
     */
    protected $channel;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $columns = array();

    /**
     * @param MediaManager               $mediaManager
     * @param ChannelManager             $channelManager
     * @param Connection                 $connection
     * @param ItemExpandLocalesConverter $itemExpandLocalesConverter
     */
    public function __construct(
        MediaManager $mediaManager,
        ChannelManager $channelManager,
        Connection $connection,
        ItemExpandLocalesConverter $itemExpandLocalesConverter
    ) {
        $this->channelManager = $channelManager;
        $this->connection = $connection;
        $this->schemaManager = $connection->getSchemaManager();
        $this->itemExpandLocalesConverter = $itemExpandLocalesConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function setStepExecution(StepExecution $stepExecution)
    {
        parent::setStepExecution($stepExecution);

        $this->table = sprintf('EZC_%s', $stepExecution->getJobExecution()->getJobInstance()->getCode());
        if ($this->schemaManager->tablesExist($this->table)) {
            $this->schemaManager->dropTable($this->table);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function write(array $items)
    {
        parent::write($items);

        foreach ($this->items as $item) {
            $localizedItems = $this->itemExpandLocalesConverter->convert(
                $item,
                $this->stepExecution->getExecutionContext()->get('category'),
                $this->channel
            );
            foreach ($localizedItems as $localizedItem) {
                try {
                    $this->requireColumns(array_keys($localizedItem));
                    $this->insertItem($localizedItem);
                } catch (\Doctrine\DBAL\ConnectionException $e) {
                }
            }
        }
        $this->items = array();
    }

    /**
     * {@inheritdoc}
     */
    public function initialize()
    {
        $this->connection->getConfiguration()->setSQLLogger(null);

        if (!is_object($this->channel)) {
            $this->channel = $this->channelManager->getChannelByCode($this->channel);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigurationFields()
    {
        return [
            'channel' => [
                'type'    => 'choice',
                'options' => [
                    'choices'  => $this->channelManager->getChannelChoices(),
                    'required' => true,
                    'select2'  => true,
                    'label'    => 'pim_base_connector.export.channel.label',
                    'help'     => 'pim_base_connector.export.channel.help'
                ]
            ]
        ];
    }

    /**
     * @param string $channel Channel code
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return Channel|string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Look for unknown columns within a list, and trigger creation of these columns.
     *
     * @param array $columns
     */
    protected function requireColumns(array $columns)
    {
        $adding = array();
        foreach ($columns as $name) {
            $name = preg_replace('/[^a-z0-9_]/', '_', strtolower($name));
            if (!in_array($name, $this->columns)) {
                $this->columns[] = $name;
                $adding[] = $name;
            }
        }

        if (count($adding) > 0) {
            $this->addColumns($adding);
        }
    }

    /**
     * Add columns to the table. If the table does not exist, it will be created.
     *
     * @param array $columns
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function addColumns(array $columns)
    {
        $added = array();
        foreach ($columns as $name) {
            $added[] = new Column($name, Type::getType(Type::TEXT), array('Notnull' => false));
        }

        if (!$this->schemaManager->tablesExist($this->table)) {
            $this->schemaManager->createTable(new Table($this->table, $added));
        } else {
            $this->schemaManager->alterTable(new TableDiff($this->table, $added));
        }
    }

    /**
     * Trigger row insertion in table.
     *
     * @param array $rawItem
     */
    protected function insertItem(array $rawItem)
    {
        $item = array();
        foreach ($rawItem as $property => $value) {
            $item[preg_replace('/[^a-z0-9_]/', '_', strtolower($property))] = $value;
        }

        $this->connection->insert($this->table, $item);
    }
}
