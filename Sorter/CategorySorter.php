<?php

namespace Naolis\Bundle\ConnectorBundle\Sorter;

use Pim\Bundle\CatalogBundle\Entity\Repository\CategoryRepository;
use Pim\Bundle\CatalogBundle\Manager\ChannelManager;

/**
 * Sorter to sort line using categories
 *
 * @author Aymeric Morilleau <am@naolis.com>
 * @copyright 2015 Naolis SARL (http://www.naolis.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CategorySorter implements SorterInterface
{
    /** @staticvar string */
    const FIELD_SORT = 'category_%d';

    /**
     * @var ChannelManager
     */
    protected $channelManager;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var array
     */
    protected $sortReferences;

    /**
     * @param ChannelManager $channelManager
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(ChannelManager $channelManager, CategoryRepository $categoryRepository)
    {
        $this->channelManager = $channelManager;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param array $data
     * @param array $context
     * @return array
     */
    public function sort($data, $context)
    {
        $sortReferences = $this->getCategoryHierarchy($context['scopeCode']);

        $data = array_map(function ($entry) use ($sortReferences) {
            $entry['_sort'] = $sortReferences[$entry[self::FIELD_SORT]];
            return $entry;
        }, $data);

        usort($data, function ($entry1, $entry2) {
            if ($entry1['_sort'] == $entry2['_sort']) {
                return 0;
            }

            return ($entry1['_sort'] < $entry2['_sort']) ? -1 : 1;
        });

        $data = array_map(function ($entry) use ($sortReferences) {
            unset($entry['_sort']);
            return $entry;
        }, $data);

        return $data;
    }

    /**
     * @param string $channel
     * @return array
     */
    private function getCategoryHierarchy($channel)
    {
        $rootCategory = $this->channelManager->getChannelByCode($channel)->getCategory();

        $tree = $this->categoryRepository->getNodesHierarchyQuery($rootCategory)->getResult();

        $categoryHierarchy = array_flip(array_map(
            function ($entry) {
                $path = array();
                $categories = $this->categoryRepository->getPath($entry);
                foreach ($categories as $category) {
                    if (!$category->isRoot()) {
                        $path[] = $category->getCode();
                    }
                }
                return implode('|', $path);
            },
            array_values($tree)
        ));

        return $categoryHierarchy;
    }
}
