<?php

namespace Naolis\Bundle\ConnectorBundle\Processor;

use Pim\Bundle\BaseConnectorBundle\Processor\CsvSerializer\ProductProcessor as BaseProductProcessor;
use Naolis\Bundle\ConnectorBundle\Sorter\SorterInterface;

/**
 * Product serializer into csv processor
 * Extends base processor to can :
 * - use ncsv format
 * - define Sorter to use
 *
 * @author Aymeric Morilleau <am@naolis.com>
 * @copyright 2015 Naolis SARL (http://www.naolis.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CsvProductProcessor extends BaseProductProcessor
{

    /**
     * @var SorterInterface
     */
    protected $sorter;

    /**
     * {@inheritdoc}
     */
    public function process($products)
    {
        $csv =  $this->serializer->serialize(
            $products,
            'ncsv',
            array(
                'delimiter'         => $this->delimiter,
                'enclosure'         => $this->enclosure,
                'withHeader'        => $this->withHeader,
                'heterogeneous'     => true,
                'scopeCode'         => $this->channel,
                'localeCodes'       => $this->getLocaleCodes($this->channel),
                'sorter'            => $this->sorter,
                'rootCategory'      => $this->channelManager->getChannelByCode($this->channel)->getCategory()
            )
        );

        $nbItems = count(explode(PHP_EOL, trim($csv))) - ($this->isWithHeader() ? 1 : 0);
        $this->stepExecution->addSummaryInfo('write', $nbItems);

        if (!is_array($products)) {
            $products = array($products);
        }

        $media = array();
        foreach ($products as $product) {
            $media = array_merge($product->getMedia(), $media);
        }

        return array(
            'entry' => $csv,
            'media' => $media
        );
    }

    public function setSorter($sorter)
    {
        $this->sorter = $sorter;
    }
}
