<?php

namespace Naolis\Bundle\ConnectorBundle\Processor;

/**
 * ProductPorcessor to handle database export
 *
 * @author Aymeric Morilleau <am@naolis.com>
 * @copyright 2015 Naolis SARL (http://www.naolis.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class TableProductProcessor extends CsvProductProcessor
{
    /**
     * {@inheritdoc}
     */
    public function process($products)
    {
        $data = parent::process($products);

         return array_merge($data, array(
             'channel' => $this->channel
         ));
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigurationFields()
    {
        return array(
            'channel' => array(
                'type'    => 'choice',
                'options' => array(
                    'choices'  => $this->channelManager->getChannelChoices(),
                    'required' => true,
                    'select2'  => true,
                    'label'    => 'pim_base_connector.export.channel.label',
                    'help'     => 'pim_base_connector.export.channel.help'
                )
            )
        );
    }
}
