<?php

namespace Naolis\Bundle\ConnectorBundle;

use Akeneo\Bundle\BatchBundle\Connector\Connector;

 /**
 *
 * @author Aymeric Morilleau <am@naolis.com>
 * @copyright 2015 Naolis SARL (http://www.naolis.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
class NaolisConnectorBundle extends Connector
{
}
