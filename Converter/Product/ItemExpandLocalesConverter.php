<?php

namespace Naolis\Bundle\ConnectorBundle\Converter\Product;

use Pim\Bundle\TransformBundle\Normalizer\Flat\ProductNormalizer;
use Pim\Bundle\CatalogBundle\Entity\Category;
use Pim\Bundle\CatalogBundle\Entity\Channel;

class ItemExpandLocalesConverter
{
    /**
     * Convert a product item row to as such as rows as there is locales in the export channel.
     *
     * @param array    $item
     * @param Category $category
     * @param Channel  $channel
     *
     * @return array
     */
    public function convert(array $item, Category $category, Channel $channel)
    {
        unset($item[ProductNormalizer::FIELD_CATEGORY]);
        $path = $this->getCategoriesPath($category);

        $lines = array();
        foreach ($channel->getLocaleCodes() as $locale) {
            $lines[$locale] = array(
                'locale' => $locale
            );

            $i = 0;
            foreach ($path as $category) {
                $i++;
                $lines[$locale][sprintf('category_%d_code', $i)]  = $category->getCode();
                $lines[$locale][sprintf('category_%d_label', $i)] = $category->getTranslation($locale)->getLabel();
            }

            foreach ($item as $column => $value) {
                if ($column === ProductNormalizer::FIELD_CATEGORY) {
                    continue;
                }

                if (is_array($value)) {
                    if (isset($value[$locale])) {
                        $lines[$locale][$column] = $value[$locale];
                    } else {
                        $lines[$locale][$column] = null;
                    }
                } else {
                    $matches = array();
                    $localeKey = $locale;
                    $columnKey = $column;
                    if (preg_match('/^([a-zA-Z0-9_]+)-([a-z]{2}_[A-Z]{2})-([a-zA-Z0-9_]+)$/', $column, $matches)) {
                        list($text, $columnKey, $localeKey, $channel) = $matches;
                    } elseif (preg_match('/^([a-zA-Z0-9_]+)-([a-z]{2}_[A-Z]{2})$/', $column, $matches)) {
                        list($text, $columnKey, $localeKey) = $matches;
                    } elseif (preg_match('/^([a-zA-Z0-9_]+)-([a-zA-Z0-9_]+)$/', $column, $matches)) {
                        list($text, $columnKey, $channel) = $matches;
                    }

                    $lines[$localeKey][$columnKey] = $value;
                }
            }
        }

        return $lines;
    }

    /**
     * Get the path of categories to access the one provided.
     *
     * @param Category $category
     *
     * @return Category[]
     */
    protected function getCategoriesPath(Category $category)
    {
        $path = array();
        while (!$category->isRoot()) {
            $path[] = $category;
            $category = $category->getParent();
        }

        return array_reverse($path);
    }
}
