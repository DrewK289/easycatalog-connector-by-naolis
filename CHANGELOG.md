# 1.2
## Features
- Rewrite all component to be able to export large data.
- Category are export in code and label to allow grouping.
- Label from all attributes are now used.

# 1.1
## Features (Only Akeneo 1.3+)
- SimpleSelect data now output with label instead of code value
- Category data now output with label instead of code value

# 1.0
## Features
- Create output following the category structure in Akeneo
- Place product for each selected place in the category
- Create one line of the output for each local of the channel
- Remove local and channel suffixe of Fields names
- Create one column for each level in the category allowing easycatalog to group on them